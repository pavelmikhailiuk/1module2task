package by.epam.volume.main;

import by.epam.volume.figure.ComplexFigure;
import by.epam.volume.figure.impl.Octahedron;

/**
 * Created by Pavel on 7/9/2016.
 */
public class Runner {
	public static void main(String[] args) {
		ComplexFigure figure = new Octahedron(13.5, 18.4, 10.8);
		System.out.println("Volume=" + figure.calculateVolume());
	}
}

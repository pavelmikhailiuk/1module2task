package by.epam.volume.figure;

import java.util.List;

public abstract class ComplexFigure implements Figure {

	protected List<Figure> components;

	public double calculateVolume() {
		if (components == null) {
			split();
		}
		double volume = 0;
		for (Figure figure : components) {
			volume = figure.add() ? volume + figure.calculateVolume() : volume - figure.calculateVolume();
		}
		return volume;
	}

	public abstract void split();

	public List<Figure> getComponents() {
		return components;
	}

	public void setComponents(List<Figure> components) {
		this.components = components;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((components == null) ? 0 : components.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComplexFigure other = (ComplexFigure) obj;
		if (components == null) {
			if (other.components != null)
				return false;
		} else if (!components.equals(other.components))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ComplexFigure [components=" + components + "]";
	}
}

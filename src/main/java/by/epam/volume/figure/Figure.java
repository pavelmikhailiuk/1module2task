package by.epam.volume.figure;

/**
 * Created by Pavel on 7/9/2016.
 */
public interface Figure {
	double calculateVolume();

	boolean add();
}

package by.epam.volume.figure.impl;

import by.epam.volume.figure.Figure;

/**
 * Created by Pavel on 7/9/2016.
 */
public class Pyramid implements Figure {

	private double height;
	private double depth;
	private double width;
	private boolean add;

	public Pyramid(double height, double depth, double width, boolean add) {
		this.height = height;
		this.depth = depth;
		this.width = width;
		this.add = add;
	}

	public double calculateVolume() {
		return width * depth * height / 3;
	}

	public boolean add() {
		return add;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getDepth() {
		return depth;
	}

	public void setDepth(double depth) {
		this.depth = depth;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (add ? 1231 : 1237);
		long temp;
		temp = Double.doubleToLongBits(depth);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(height);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(width);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pyramid other = (Pyramid) obj;
		if (add != other.add)
			return false;
		if (Double.doubleToLongBits(depth) != Double.doubleToLongBits(other.depth))
			return false;
		if (Double.doubleToLongBits(height) != Double.doubleToLongBits(other.height))
			return false;
		if (Double.doubleToLongBits(width) != Double.doubleToLongBits(other.width))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Pyramid [height=" + height + ", depth=" + depth + ", width=" + width + ", add=" + add + "]";
	}
}
